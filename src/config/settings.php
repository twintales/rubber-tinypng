<?
	return array(
		/**
		 * The api key to use the service.
		 */
		'api_key' => array(),

		/**
		 * number of allowed transaction ?
		 */
		'rate_limit' => 500,

		/**
		 * Connection type: curl or fopen
		 */
		'connection' => 'curl',

		/* set this the path to a cert file downloadable from this url below, or see tinypng site for update about this
		 Download cacert.pem from: http://curl.haxx.se/ca/cacert.pem */
		'ssl_cert' => null
	);
?>