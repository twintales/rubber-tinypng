<? namespace Rubber\TinyPNG;
	class Optimizer{

		protected $key = null;
		protected $rate = null;
		protected $connection = null;
		protected $url = "https://api.tinypng.com/shrink";
		protected $ssl_cert = null;

		public function __construct( $config )
		{
			$this->key = $config['api_key'];
			$this->rate = $config['rate_limit'];
			$this->connection = $config['connection'];
			$this->ssl_cert = $config['ssl_cert'];
		}

		public function __set($name, $value)
		{
			echo "Setting '$name' to '$value'\n";
			$this->data[$name] = $value;
		}

		public function __get($name)
		{
			echo "Getting '$name'\n";
			if (array_key_exists($name, $this->data)) {
			    return $this->data[$name];
		}

			$trace = debug_backtrace();
			trigger_error(
			'Undefined property via __get(): ' . $name .
			' in ' . $trace[0]['file'] .
			' on line ' . $trace[0]['line'],
			E_USER_NOTICE);
			return null;
		}

		public function optimized(){
			try{
				if($this->connection == 'curl')
				{
					$this->_curl();
				}
				elseif($this->connection == 'fopen')
				{
					$this->_fopen();
				}
				else
				{

				}
			}
			catch (Exception\BaseException $exception){
				return $exception;
			}
			catch (Exception $exception){
				return $exception;
			}
		}

		protected function _curl($url){

		}

		protected function _fopen($url){
			$options = array(
				"http" => array(
					"method" => "POST",
					"header" => array(
						"Content-type: image/png",
						"Authorization: Basic " . base64_encode("api:$key")
					),
					"content" => file_get_contents($input)
				),
				"ssl" => array(
					"verify_peer" => true
				)
			);
			/**
			 * Addiitonal SSL cert file when SSL verify has problem. see setting
			 */
			if(!empty($this->sslCert))
			{
				//__DIR__ . "/cacert.pem",
				$options['ssl']['cafile'] => $this->ssl_cert
			}

			$result = fopen($url, "r", false, stream_context_create($options));
			if ($result)
			{
				/* Compression was successful, retrieve output from Location header. */
				foreach ($http_response_header as $header)
				{
					if (substr($header, 0, 10) === "Location: ")
					{
						file_put_contents($output, fopen(substr($header, 10), "rb", false));
					}
				}
			}
			else
			{
				return new Exception\CompressionFailedException;
			}
		}

	}
?>