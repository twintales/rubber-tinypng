<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TinypngCreateTransactionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tinypng_history', function($table) {
			$table->increments('id');
			$table->string('original_file');
			$table->string('optimized_file');
			$table->text('response');
			$table->string('ip', 100);
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tinypng_history');
	}

}
