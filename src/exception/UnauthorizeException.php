<?php namespace Rubber\TinyPNG\Exception;

	class UnauthorizeException extends BaseException{
		protected $message = 'The request was not authorized with a valid API key.';
	}

?>