<?php namespace Rubber\TinyPNG\Exception;

	class BadSignatureException extends BaseException{
		protected $message = 'The file was not recognized as a PNG file. It may be corrupted or it is a different file type.';
	}

?>