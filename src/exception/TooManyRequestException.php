<?php namespace Rubber\TinyPNG\Exception;

	class TooManyRequestsException extends BaseException{
		protected $message = 'Your monthly upload limit has been exceeded. Either wait until the next calendar month, or upgrade your subscription.';
	}

?>