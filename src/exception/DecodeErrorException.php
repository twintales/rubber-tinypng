<?php namespace Rubber\TinyPNG\Exception;

	class DecodeErrorException extends BaseException{
		protected $message = 'The file had a valid PNG signature, but could not be decoded. It may be corrupted or is of an unsupported type. If you are positive you are sending a PNG file, feel free to contact us and send the file to us for diagnostics.';
	}

?>