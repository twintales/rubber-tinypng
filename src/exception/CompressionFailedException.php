<?php namespace Rubber\TinyPNG\Exception;

	class InternalServerErrorException extends BaseException{
		protected $message = 'An internal error occurred during conversion. This error is usually temporary. If the uploaded file is a valid PNG file, you can try again later.';
	}

?>