<?php namespace Rubber\TinyPNG\Exception;

	class InputMissingException extends BaseException{
		protected $message = 'The file that was uploaded is empty or no data was posted.';
	}

?>