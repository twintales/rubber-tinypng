<?php namespace Rubber\TinyPNG\Exception;

	class CompressionFailedException extends BaseException{
		protected $message = 'The compression has failed.';
	}

?>